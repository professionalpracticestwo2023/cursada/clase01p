import os.path
if (os.path.isfile("../data/modelos.dat")):
        os.remove("../data/modelos.dat")    
try:
    id_model=1
    file = open ("../data/test.data","r")
    gender = open("../data/modelos.dat","a+")
    gender.close()
    lines = file.readlines()
    for line in lines:
        datafile = line.split("|")
        model = open("../data/modelos.dat","r")
        models = model.readlines()
        found = 0
        for record in models:
            if len(record)>1:
                datamodel = record.split("|")
                if datafile[16].rstrip() == datamodel[1].rstrip():
                    found=1
                    break
        model.close()
        if found == 0:
            model = open("../data/modelos.dat","a+")
            model.write(str(id_model)+'|'+datafile[16].rstrip()+'\n')
            model.close()
            id_model+=1
    file.close()
except FileNotFoundError:
    print("No se puede continuar, error en la apertura de archivos")
    exit()